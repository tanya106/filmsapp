namespace FilmsApp
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.AspNetCore.SpaServices.Webpack;
    using Microsoft.AspNetCore.Authentication.Cookies;
    using FilmsApp.Services.Users;
    using FilmsApp.Data;
    using Microsoft.EntityFrameworkCore;
    using FilmsApp.Services.Films;
    using FilmsApp.Services.Films.Helpers;
    using FilmsApp.Services.Blobs;

    /// <summary>
    /// Startup class defined in Program.cs.
    /// </summary>
    public class Startup
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// Constructor with injected configuration.
        /// </summary>
        /// <param name="configuration">Injected param. Is used to get some properties from configuration file.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">Injected param. Provides collection of services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetConnectionString("AzureDatabaseConnectionString");
            services.AddDbContext<FilmsDbContext>(options =>
                options.UseSqlServer(connection));

            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IFilmsService, FilmsService>();
            services.AddTransient<IFilmsHelper, FilmsHelper>();
            services.AddTransient<IBlobService, BlobService>();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
               .AddCookie(options => //CookieAuthenticationOptions
                {
                    options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login");
                });
            services.AddMvc();

        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">Injected param. ApplicationBuilder.</param>
        /// <param name="env">Injected param. Provides information about environment.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMvc(routes =>
                {
                    routes.MapRoute("default", "{controller=Home}/{action=Index}");
                });

            if (env.IsDevelopment())
            {
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
               
            app.UseAuthentication();
            app.UseDefaultFiles();
            app.UseStaticFiles();

        }
    }
}
