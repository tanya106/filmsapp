using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using FilmsApp.Data.DatabaseModels;

namespace FilmsApp.Data
{
    public partial class FilmsDbContext : DbContext
    {
        public FilmsDbContext(DbContextOptions<FilmsDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<ClientFilmAdded> ClientFilmAdded { get; set; }
        public virtual DbSet<ClientFilmComment> ClientFilmComment { get; set; }
        public virtual DbSet<ClientFilmDelayed> ClientFilmDelayed { get; set; }
        public virtual DbSet<ClientFilmFavourite> ClientFilmFavourite { get; set; }
        public virtual DbSet<ClientFilmRate> ClientFilmRate { get; set; }
        public virtual DbSet<ClientFilmWatched> ClientFilmWatched { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Film> Film { get; set; }
        public virtual DbSet<FilmActor> FilmActor { get; set; }
        public virtual DbSet<FilmCategory> FilmCategory { get; set; }
        public virtual DbSet<FilmCountry> FilmCountry { get; set; }
        public virtual DbSet<FilmParticipant> FilmParticipant { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserPassword> UserPassword { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ClientFilmAdded>(entity =>
            {
                entity.HasKey(e => new { e.FilmId, e.Username });

                entity.HasIndex(e => e.Username);

                entity.Property(e => e.FilmId).HasColumnName("filmId");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(50);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(50);

                entity.HasOne(d => d.Film)
                    .WithMany(p => p.ClientFilmAdded)
                    .HasForeignKey(d => d.FilmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientFilmAdded_Film");

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.ClientFilmAdded)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientFilmAdded_User");
            });

            modelBuilder.Entity<ClientFilmComment>(entity =>
            {
                entity.HasKey(e => new { e.FilmId, e.Username, e.Date });

                entity.HasIndex(e => e.Username);

                entity.Property(e => e.FilmId).HasColumnName("filmId");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(50);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasColumnName("comment")
                    .HasMaxLength(1000);

                entity.HasOne(d => d.Film)
                    .WithMany(p => p.ClientFilmComment)
                    .HasForeignKey(d => d.FilmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientFilmComment_Film");

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.ClientFilmComment)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientFilmComment_User");
            });

            modelBuilder.Entity<ClientFilmDelayed>(entity =>
            {
                entity.HasKey(e => new { e.FilmId, e.Username });

                entity.HasIndex(e => e.Username);

                entity.Property(e => e.FilmId).HasColumnName("filmId");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(50);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.HasOne(d => d.Film)
                    .WithMany(p => p.ClientFilmDelayed)
                    .HasForeignKey(d => d.FilmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientFilmDelayed_Film");

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.ClientFilmDelayed)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientFilmDelayed_User");
            });

            modelBuilder.Entity<ClientFilmFavourite>(entity =>
            {
                entity.HasKey(e => new { e.FilmId, e.Username });

                entity.HasIndex(e => e.Username);

                entity.Property(e => e.FilmId).HasColumnName("filmId");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(50);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.HasOne(d => d.Film)
                    .WithMany(p => p.ClientFilmFavourite)
                    .HasForeignKey(d => d.FilmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientFilmFavourite_Film");

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.ClientFilmFavourite)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientFilmFavourite_User");
            });

            modelBuilder.Entity<ClientFilmRate>(entity =>
            {
                entity.HasKey(e => new { e.FilmId, e.Username });

                entity.HasIndex(e => e.Username);

                entity.Property(e => e.FilmId).HasColumnName("filmId");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(50);

                entity.Property(e => e.Rate).HasColumnName("rate");

                entity.HasOne(d => d.Film)
                    .WithMany(p => p.ClientFilmRate)
                    .HasForeignKey(d => d.FilmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientFilmRate_Film");

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.ClientFilmRate)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientFilmRate_User");
            });

            modelBuilder.Entity<ClientFilmWatched>(entity =>
            {
                entity.HasKey(e => new { e.FilmId, e.Username });

                entity.HasIndex(e => e.Username);

                entity.Property(e => e.FilmId).HasColumnName("filmId");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(50);

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.HasOne(d => d.Film)
                    .WithMany(p => p.ClientFilmWatched)
                    .HasForeignKey(d => d.FilmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientFilmWatched_Film");

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.ClientFilmWatched)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ClientFilmWatched_User");
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Film>(entity =>
            {
                entity.HasIndex(e => e.CameraMan);

                entity.HasIndex(e => e.EditedBy);

                entity.HasIndex(e => e.MusicBy);

                entity.HasIndex(e => e.Producer);

                entity.HasIndex(e => e.ScriptWriter);

                entity.HasIndex(e => e.StageManager);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AuthorLink)
                    .IsRequired()
                    .HasColumnName("authorLink")
                    .HasMaxLength(150);

                entity.Property(e => e.CameraMan).HasColumnName("cameraMan");

                entity.Property(e => e.CountRated).HasColumnName("countRated");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.EditedBy).HasColumnName("editedBy");

                entity.Property(e => e.MinuteDuration).HasColumnName("minuteDuration");

                entity.Property(e => e.MusicBy).HasColumnName("musicBy");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.Producer).HasColumnName("producer");

                entity.Property(e => e.ProductionYear).HasColumnName("productionYear");

                entity.Property(e => e.Rate).HasColumnName("rate");

                entity.Property(e => e.ScriptWriter).HasColumnName("scriptWriter");

                entity.Property(e => e.StageManager).HasColumnName("stageManager");

                entity.Property(e => e.VideoFile)
                    .HasColumnName("videoFile")
                    .HasMaxLength(128);

                entity.HasOne(d => d.CameraManNavigation)
                    .WithMany(p => p.FilmCameraManNavigation)
                    .HasForeignKey(d => d.CameraMan)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Film_FilmParticipant4");

                entity.HasOne(d => d.EditedByNavigation)
                    .WithMany(p => p.FilmEditedByNavigation)
                    .HasForeignKey(d => d.EditedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Film_FilmParticipant5");

                entity.HasOne(d => d.MusicByNavigation)
                    .WithMany(p => p.FilmMusicByNavigation)
                    .HasForeignKey(d => d.MusicBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Film_FilmParticipant");

                entity.HasOne(d => d.ProducerNavigation)
                    .WithMany(p => p.FilmProducerNavigation)
                    .HasForeignKey(d => d.Producer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Film_FilmParticipant1");

                entity.HasOne(d => d.ScriptWriterNavigation)
                    .WithMany(p => p.FilmScriptWriterNavigation)
                    .HasForeignKey(d => d.ScriptWriter)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Film_FilmParticipant2");

                entity.HasOne(d => d.StageManagerNavigation)
                    .WithMany(p => p.FilmStageManagerNavigation)
                    .HasForeignKey(d => d.StageManager)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Film_FilmParticipant3");
            });

            modelBuilder.Entity<FilmActor>(entity =>
            {
                entity.HasKey(e => new { e.FilmId, e.ActorId });

                entity.HasIndex(e => e.ActorId);

                entity.Property(e => e.FilmId).HasColumnName("filmId");

                entity.Property(e => e.ActorId).HasColumnName("actorId");

                entity.HasOne(d => d.Actor)
                    .WithMany(p => p.FilmActor)
                    .HasForeignKey(d => d.ActorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FilmActor_FilmParticipant");

                entity.HasOne(d => d.Film)
                    .WithMany(p => p.FilmActor)
                    .HasForeignKey(d => d.FilmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FilmActor_Film");
            });

            modelBuilder.Entity<FilmCategory>(entity =>
            {
                entity.HasKey(e => new { e.FilmId, e.CategoryId });

                entity.HasIndex(e => e.CategoryId);

                entity.Property(e => e.FilmId).HasColumnName("filmId");

                entity.Property(e => e.CategoryId).HasColumnName("categoryId");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.FilmCategory)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FilmCategory_Category");

                entity.HasOne(d => d.Film)
                    .WithMany(p => p.FilmCategory)
                    .HasForeignKey(d => d.FilmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FilmCategory_Film");
            });

            modelBuilder.Entity<FilmCountry>(entity =>
            {
                entity.HasKey(e => new { e.FilmId, e.CountryId });

                entity.HasIndex(e => e.CountryId);

                entity.Property(e => e.FilmId).HasColumnName("filmId");

                entity.Property(e => e.CountryId).HasColumnName("countryId");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.FilmCountry)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FilmCountry_Country");

                entity.HasOne(d => d.Film)
                    .WithMany(p => p.FilmCountry)
                    .HasForeignKey(d => d.FilmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_FilmCountry_Film");
            });

            modelBuilder.Entity<FilmParticipant>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FatherName)
                    .IsRequired()
                    .HasColumnName("fatherName")
                    .HasMaxLength(50);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnName("firstName")
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Username);

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.BirthDate)
                    .HasColumnName("birthDate")
                    .HasColumnType("date");

                entity.Property(e => e.ConnectionLink)
                    .HasColumnName("connectionLink")
                    .HasMaxLength(150);

                entity.Property(e => e.FatherName)
                    .HasColumnName("fatherName")
                    .HasMaxLength(50);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnName("firstName")
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("lastName")
                    .HasMaxLength(50);

                entity.Property(e => e.Mail)
                    .IsRequired()
                    .HasColumnName("mail")
                    .HasMaxLength(50);

                entity.Property(e => e.ProfileImage)
                    .HasColumnName("profileImage")
                    .HasMaxLength(150);

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasColumnName("role")
                    .HasMaxLength(50);

                entity.Property(e => e.Sex)
                    .IsRequired()
                    .HasColumnName("sex")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<UserPassword>(entity =>
            {
                entity.HasKey(e => e.Username);

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(50)
                    .ValueGeneratedNever();

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(128);

                entity.HasOne(d => d.UsernameNavigation)
                    .WithOne(p => p.UserPassword)
                    .HasForeignKey<UserPassword>(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserPassword_User");
            });
        }
    }
}
