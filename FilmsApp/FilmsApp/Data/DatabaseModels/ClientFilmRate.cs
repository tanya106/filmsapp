﻿using System;
using System.Collections.Generic;

namespace FilmsApp.Data.DatabaseModels
{
    public partial class ClientFilmRate
    {
        public long FilmId { get; set; }
        public string Username { get; set; }
        public double Rate { get; set; }

        public Film Film { get; set; }
        public User UsernameNavigation { get; set; }
    }
}
