﻿using System;
using System.Collections.Generic;

namespace FilmsApp.Data.DatabaseModels
{
    public partial class FilmParticipant
    {
        public FilmParticipant()
        {
            FilmActor = new HashSet<FilmActor>();
            FilmCameraManNavigation = new HashSet<Film>();
            FilmEditedByNavigation = new HashSet<Film>();
            FilmMusicByNavigation = new HashSet<Film>();
            FilmProducerNavigation = new HashSet<Film>();
            FilmScriptWriterNavigation = new HashSet<Film>();
            FilmStageManagerNavigation = new HashSet<Film>();
        }

        public long Id { get; set; }
        public string FatherName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public ICollection<FilmActor> FilmActor { get; set; }
        public ICollection<Film> FilmCameraManNavigation { get; set; }
        public ICollection<Film> FilmEditedByNavigation { get; set; }
        public ICollection<Film> FilmMusicByNavigation { get; set; }
        public ICollection<Film> FilmProducerNavigation { get; set; }
        public ICollection<Film> FilmScriptWriterNavigation { get; set; }
        public ICollection<Film> FilmStageManagerNavigation { get; set; }
    }
}
