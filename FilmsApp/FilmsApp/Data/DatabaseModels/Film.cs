using System;
using System.Collections.Generic;

namespace FilmsApp.Data.DatabaseModels
{
    public partial class Film
    {
        public Film()
        {
            ClientFilmAdded = new HashSet<ClientFilmAdded>();
            ClientFilmComment = new HashSet<ClientFilmComment>();
            ClientFilmDelayed = new HashSet<ClientFilmDelayed>();
            ClientFilmFavourite = new HashSet<ClientFilmFavourite>();
            ClientFilmRate = new HashSet<ClientFilmRate>();
            ClientFilmWatched = new HashSet<ClientFilmWatched>();
            FilmActor = new HashSet<FilmActor>();
            FilmCategory = new HashSet<FilmCategory>();
            FilmCountry = new HashSet<FilmCountry>();
        }

        public long Id { get; set; }
        public int AgePermition { get; set; }
        public string AuthorLink { get; set; }
        public long CameraMan { get; set; }
        public long EditedBy { get; set; }
        public int MinuteDuration { get; set; }
        public long MusicBy { get; set; }
        public string Name { get; set; }
        public long Producer { get; set; }
        public int ProductionYear { get; set; }
        public long ScriptWriter { get; set; }
        public long StageManager { get; set; }
        public string VideoFile { get; set; }
        public DateTime Date { get; set; }
        public double Rate { get; set; }
        public long CountRated { get; set; }
        //public string Description { get; set; }
        //public string PhotoFile { get; set; }

        public FilmParticipant CameraManNavigation { get; set; }
        public FilmParticipant EditedByNavigation { get; set; }
        public FilmParticipant MusicByNavigation { get; set; }
        public FilmParticipant ProducerNavigation { get; set; }
        public FilmParticipant ScriptWriterNavigation { get; set; }
        public FilmParticipant StageManagerNavigation { get; set; }
        public ICollection<ClientFilmAdded> ClientFilmAdded { get; set; }
        public ICollection<ClientFilmComment> ClientFilmComment { get; set; }
        public ICollection<ClientFilmDelayed> ClientFilmDelayed { get; set; }
        public ICollection<ClientFilmFavourite> ClientFilmFavourite { get; set; }
        public ICollection<ClientFilmRate> ClientFilmRate { get; set; }
        public ICollection<ClientFilmWatched> ClientFilmWatched { get; set; }
        public ICollection<FilmActor> FilmActor { get; set; }
        public ICollection<FilmCategory> FilmCategory { get; set; }
        public ICollection<FilmCountry> FilmCountry { get; set; }
    }
}
