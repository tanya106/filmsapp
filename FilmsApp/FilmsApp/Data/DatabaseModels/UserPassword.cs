﻿using System;
using System.Collections.Generic;

namespace FilmsApp.Data.DatabaseModels
{
    public partial class UserPassword
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public User UsernameNavigation { get; set; }
    }
}
