﻿using System;
using System.Collections.Generic;

namespace FilmsApp.Data.DatabaseModels
{
    public partial class Category
    {
        public Category()
        {
            FilmCategory = new HashSet<FilmCategory>();
        }

        public long Id { get; set; }
        public string Name { get; set; }

        public ICollection<FilmCategory> FilmCategory { get; set; }
    }
}
