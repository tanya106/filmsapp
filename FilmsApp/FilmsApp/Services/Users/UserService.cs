using FilmsApp.Data;
using FilmsApp.Data.DatabaseModels;
using FilmsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Services.Users
{
    public class UserService: IUserService
    {
        private FilmsDbContext dbContext;

        public UserService(FilmsDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public UserModel Login(LoginModel loginModel)
        {
            UserModel userModel = null;
            var user = dbContext.User.Find(loginModel.Username);
            if(user != null)
            {
                var loginIsOk = dbContext.UserPassword.Find(loginModel.Username).Password == loginModel.Password;
                if(loginIsOk)
                {
                    userModel = new UserModel()
                    {
                        Username = user.Username,
                        Role = Convert.ToInt64(user.Role)
                    };
                }
            }
            return userModel;
        }

        public UserModel SignUp(SignUpModel signUpModel)
        {
            UserModel userModel = null;
            if (dbContext.User.Find(signUpModel.Username) != null)
            {
                throw new Exception("Пользователь с таким логином уже существует");
            }
            var user = new User()
            {
              Username = signUpModel.Username,
              BirthDate = signUpModel.BirthDate,
              ConnectionLink = signUpModel.ConnectionLink,
              FatherName = signUpModel.FatherName,
              FirstName = signUpModel.FirstName,
              LastName = signUpModel.LastName,
              Mail = signUpModel.Mail,
              ProfileImage = signUpModel.ProfileImage,
              Sex = signUpModel.Sex,
              Role = "1"
            };
            var userCreated = dbContext.User.Add(user);
            dbContext.SaveChanges();
            if(userCreated != null)
            {
                var password = new UserPassword()
                {
                  Username = signUpModel.Username,
                  Password = signUpModel.Password
                };
                var passwordCreated = dbContext.UserPassword.Add(password);
                dbContext.SaveChanges();
                if (passwordCreated != null)
                {
                    userModel = new UserModel()
                    {
                        Username = user.Username,
                        Role = Convert.ToInt64(user.Role)
                    };
                }
                else
                {
                    dbContext.User.Remove(user);
                    dbContext.SaveChanges();
                }
            }

            return userModel;
        }
    }
}
