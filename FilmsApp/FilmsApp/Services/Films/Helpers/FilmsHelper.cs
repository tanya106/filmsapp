using FilmsApp.Data;
using FilmsApp.Data.DatabaseModels;
using FilmsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Services.Films.Helpers
{
    public class FilmsHelper: IFilmsHelper
    {
        private FilmsDbContext dbContext;

        public FilmsHelper(FilmsDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public Film FindFilm(long filmId)
        {
            var film = dbContext.Film.Find(filmId);
            if (film == null)
            {
              throw new Exception("There is no element with such id in the database");
            }
            return film;
            
        }

        public string GetFilmParticipentNameAndSurname(long participantId)
        {
            var participant = dbContext.FilmParticipant.Find(participantId);
            return participant.FirstName + " " + participant.LastName;
        }

        public string FindCategories(long filmId)
        {
            var filmCategories = dbContext.FilmCategory.Where(o => o.FilmId == filmId).ToList();
            var categories = "";
            foreach (var filmCategory in filmCategories)
            {
              categories += dbContext.Category.Find(filmCategory.CategoryId).Name + ", ";
            }
            
            if (categories != "")
            {
                categories = categories.Remove(categories.Length - 2, 2);
            }

            return categories;
        }
        public IEnumerable<string> FindActors(long filmId)
        {
            var filmActors = dbContext.FilmActor.Where(o => o.FilmId == filmId).ToList();
            var actors = new List<string>();
            foreach (var filmActor in filmActors)
            {
              var actor = dbContext.FilmParticipant.Find(filmActor.ActorId);
              actors.Add(actor.FirstName + " " + actor.LastName);
            }
            if (actors == null)
            {
              throw new Exception("There is no element with such id in the database");
            }
            return actors;
        }
        public IEnumerable<ClientFilmComment> FindComments(long filmId)
        {
            var comments = dbContext.ClientFilmComment.Where(o => o.FilmId == filmId).OrderByDescending(o => o.Date).ToList();
            return comments;
        }

        public IEnumerable<string> FindCountries(long filmId)
        {
            var filmCountries = dbContext.FilmCountry.Where(o => o.FilmId == filmId).ToList();
            var countries = new List<string>();
            foreach(var filmCountry in filmCountries)
            {
                var countryId = filmCountry.CountryId;
                var countryName = dbContext.Country.Find(countryId).Name;
                countries.Add(countryName);
            }
            return countries;
        }

        public long FindFilmParticipentId(FilmParticipantModel filmParticipantModel)
        {
            filmParticipantModel.FatherName = (filmParticipantModel.FatherName == null) ? "" : filmParticipantModel.FatherName;
            var ids = dbContext.FilmParticipant.Where(o => o.FirstName == filmParticipantModel.FirstName)
                                                .Where(o => o.LastName == filmParticipantModel.LastName)
                                                .Where(o => o.FatherName == filmParticipantModel.FatherName)
                                                .ToList();
            if(ids.Count == 0)
            {
                var filmParticipant = new FilmParticipant()
                {
                    FirstName = filmParticipantModel.FirstName,
                    LastName = filmParticipantModel.LastName,
                    FatherName = (filmParticipantModel.FatherName == null) ? " " : filmParticipantModel.FatherName
                };
                dbContext.FilmParticipant.Add(filmParticipant);
                dbContext.SaveChanges();
                ids = dbContext.FilmParticipant.Where(o => o.FirstName == filmParticipantModel.FirstName)
                                    .Where(o => o.LastName == filmParticipantModel.LastName)
                                    .Where(o => o.FatherName == filmParticipantModel.FatherName)
                                    .ToList();
            }

            return ids[0].Id;
        }
    }
}
