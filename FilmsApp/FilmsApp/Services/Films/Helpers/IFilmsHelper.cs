using FilmsApp.Data.DatabaseModels;
using FilmsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Services.Films.Helpers
{
    public interface IFilmsHelper
    {
        Film FindFilm(long filmId);
        string FindCategories(long filmId);
        IEnumerable<string> FindActors(long filmId);
        IEnumerable<ClientFilmComment> FindComments(long filmId);
        string GetFilmParticipentNameAndSurname(long participantId);
        IEnumerable<string> FindCountries(long filmId);
        long FindFilmParticipentId(FilmParticipantModel filmParticipentModel);
    }
}
