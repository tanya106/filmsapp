using FilmsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Services.Films
{
    public interface IFilmsService
    {
        FilmModel BuildModel(long filmId);
        IEnumerable<FilmModel> GetAccepted();
        IEnumerable<FilmModel> SearchFilm(SearchModel searchModel);
        IEnumerable<FilmModel> GetTop();
        IEnumerable<FilmModel> GetNew();
        IEnumerable<FilmModel> GetRandom();
        SearchInfoModel GetSearchInfo();
        bool AddFilm(FilmModel filmModel);
        IEnumerable<FilmModel> GetUserFilms(string username);
        IEnumerable<FilmModel> GetFilmsSuggestions();
        bool AcceptFilm(long filmId);
        double GetUserFilmRate(RateModel rateModel);
        double SetUserFilmRate(RateModel rateModel);
        bool DeleteFilm(long filmId);
        bool DenyFilm(long filmId);
        string FindFilmVideoBlob(string filmReference);

        //IEnumerable<FilmModel> GetDelayed(long userId);
        //void SetDelayed(long userId, long filmId);
        //IEnumerable<Film> GetFavourite(long userId);
        //void SetFavourite(long userId, long filmId);
        //IEnumerable<Film> GetWatched(long userId);
        //void SetWatched(long userId, long filmId);
        //IEnumerable<Film> GetAdded(long userId);
        //void SetAdded(long userId, long filmId);
    }
}
