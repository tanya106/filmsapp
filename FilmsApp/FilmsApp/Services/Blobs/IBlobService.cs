using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Services.Blobs
{
    public interface IBlobService
    {
        void UploadBlobPhoto(string reference, IFormFile file);
        void UploadBlobVideo(string reference, IFormFile file);
        Stream DownloadBlobPhoto(string reference);
        string DownloadBlobVideo(string reference);
    }
}
