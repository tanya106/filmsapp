using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Models
{
    public class RateModel
    {
        public string Username { get; set; }
        public long FilmId { get; set; }
        public double Rate { get; set; }
    }
}
