using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Models
{
    public class SearchModel
    {
        public int? YearStart { get; set; }
        public int? YearEnd { get; set; }
        public long[] CountriesId { get; set; }
        public long[] CategoriesId { get; set; }
        public long[] StageManagerId { get; set; }
        public string FilmName { get; set; }
    }
}
