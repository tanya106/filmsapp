using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Models
{
    public class CommentModel
    {
        public string Username { get; set; }
        public long? FilmId { get; set; }
        public string Comment { get; set; }
        public string Date { get; set; }
    }
}
