using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Models
{
    public class UserModel
    {
        public string Username { get; set; }
        public long Role { get; set; }
    }
}
