using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsApp.Models
{
    public class FilmModel
    {
        public long FilmId { get; set; }
        public string Name { get; set; }
        public int ProductionYear { get; set; }
        public string NameStageManager { get; set; }
        public string NameScriptWriter { get; set; }
        public string NameProducer { get; set; }
        public string NameCameraMan { get; set; }
        public string NameMusicBy { get; set; }
        public string NameEditedBy { get; set; }
        public FilmParticipantModel NameStageManagerArray { get; set; }
        public FilmParticipantModel NameScriptWriterArray { get; set; }
        public FilmParticipantModel NameProducerArray { get; set; }
        public FilmParticipantModel NameCameraManArray { get; set; }
        public FilmParticipantModel NameMusicByArray { get; set; }
        public FilmParticipantModel NameEditedByArray { get; set; }
        public int MinuteDuration { get; set; }
        public string AuthorLink { get; set; }
        public string VideoFile { get; set; }
        public string PhotoFile { get; set; }
        public string Date { get; set; }
        public int AgePermition { get; set; }
        public string Categories { get; set; }
        public List<long> CategoriesArray { get; set; }
        public IEnumerable<string> Actors { get; set; }
        public IEnumerable<FilmParticipantModel> ActorsArray { get; set; }
        public double? Rate { get; set; }
        public IEnumerable<CommentModel> Comments { get; set; }
        public IEnumerable<string> Countries { get; set; }
        public List<long> CountriesArray { get; set; }
        public string Description { get; set; }
        public IFormFile VideoFileForm { get; set; }
        public IFormFile PhotoFileForm { get; set; }
        public long Status { get; set; }
        public string Username { get; set; }

    }
}
