using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FilmsApp.Models;
using FilmsApp.Services.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
    
namespace FilmsApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        private IUserService userService;
        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }
    
        [HttpPost("login")]
        public IActionResult Login(LoginModel loginModel)
        {
            JsonResult result;
            try
            {
                var userModel = userService.Login(loginModel);
                if(userModel != null)
                {
                  result = Json(Ok(userModel));
                }
                else
                {
                  result = Json(BadRequest("User is not confirmed"));
                }
            }
            catch (Exception e)
            {
                result = Json(BadRequest(e.Message));
            }

            return result;
        }

        [HttpPost("signUp")]
        public IActionResult SignUp(SignUpModel signUpModel)
        {
          JsonResult result;
          try
          {
            var userModel = userService.SignUp(signUpModel);
            if (userModel != null)
            {
              result = Json(Ok(userModel));
            }
            else
            {
              result = Json(BadRequest(userModel));
            }
          }
          catch (Exception e)
          {
            result = Json(BadRequest(e.Message));
          }

          return result;
        }
    }
}     
