namespace FilmsApp
{
    using Microsoft.AspNetCore;
    using Microsoft.AspNetCore.Hosting;

    /// <summary>
    /// Main class of application.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Start point of application.
        /// </summary>
        /// <param name="args">Command line's params.</param>
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Defines StartUp class.
        /// </summary>
        /// <param name="args">Command line's params.</param>
        /// <returns>WebHostBuilder.</returns>
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
